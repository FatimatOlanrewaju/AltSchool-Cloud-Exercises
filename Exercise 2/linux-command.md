# 10 Linux Commands

**1. cal**

Cal is a command used by the system administrator to check and display the calendar of the current month.

![Cal Snipshot](Images/cal.png)

**2. df**

The df (disk file) command is used to see the available disk space in each of the partitions in your system. It shows each mounted partition and their used/available space in % and in KBs. To get the disk space in megabytes, the command “df -m” is used.

![DF Snipshot](Images/df.png)

**3. ping**

Ping is used to check your connection to a server. Wikipedia says, "Ping is a computer network administration software utility used to test the reachability of a host on an Internet Protocol (IP) network". Simply, when you type in, for example, “ping google.com”, it checks if it can connect to the server and come back. It measures this round-trip time and gives you the details about it. The use of this command for simple users like us is to check your internet connection. If it pings the Google server (in this case), you can confirm that your internet connection is active!

![Ping Snipshot](Images/ping.png)

**4. history**

The history command is used to review the commands you’ve entered before.

![History Snipshot](Images/history.png)

**5. htop**

htop gives an interactive process viewer that lets you manage your machine’s resources directly from the terminal.

![Htop Snipshot](Images/htop.png)

**6. grep**

Grep is one of the most powerful utilities for working with text files. It searches for lines that match a regular expression and print them.

![Grep Snipshot](Images/grep.png)

**7. Whatis**

Whatis prints a single-line description of any other command, making it a helpful reference.

![Whatis Snipshot](Images/whatis.png)

**8. WordCount**

Wc stands for “word count,” and as the name suggests, it returns the number of words in a text file.

![WordCount Snipshot](Images/wordcount.png)

**9. hostnamectl**

Hostnamectl  is the command used to get information such as kernelversion, operation system of the machine you are running, hostname, hardware model.

![Hostnamectl Snipshot](Images/hostnamectl.png)

**10. sort**

The sort command will provide a sorted output of the contents of a file. Let’s use the sort command without any parameters and see the output.

![Sort Snipshot](Images/sort.png)