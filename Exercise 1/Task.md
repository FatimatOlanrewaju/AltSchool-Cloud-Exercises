# Exercise 1

## Task:

* Setup Ubuntu 20.04 LTS on your local machine using Vagrant

## Instruction:

 - Customize your Vagrantfile as necessary with provatr_network set to 'dhcp'.
 - Once the machine is up, run ifconfig and share the output in your submission along with your Vagrantfile in a folder with this exercise.